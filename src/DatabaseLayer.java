import java.sql.*;
import java.util.ArrayList;

public class DatabaseLayer {
    private Connection connection;

    public void Initialize() throws ClassNotFoundException, SQLException
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/test","root","");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void Terminate() throws SQLException {
        if (connection != null && !connection.isClosed()) {
            connection.close();
        }
    }

    public void CreateDatabase() throws SQLException {
        String query = "{CALL create_database_if_not_exists()}";
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();
        statement.close();
    }

    public void DropDatabase() throws SQLException {
        String query = "{CALL drop_database()}";
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();
        statement.close();
    }

    public void DeleteById(int id) throws SQLException {
        String query = "{CALL delete_by_id(" + id + ")}";
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();
        statement.close();
    }

    public void InsertIntoTable(String firstName, String lastName, double averageMark)  throws SQLException {
        String query = "{CALL insert_into_table('" + firstName + "', '" + lastName + "', " + averageMark + ")}";
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();
        statement.close();
    }

    public void TruncateTable() throws SQLException {
        String query = "{CALL truncate_table()}";
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();
        statement.close();
    }

    public ArrayList<String> SelectByLastName(String p_lastName) throws SQLException {
        ArrayList<String> result = new ArrayList<>();
        Statement statement = connection.createStatement();
        String query = "{CALL select_by_last_name('" + p_lastName + "')}";
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next())
        {
            int id = resultSet.getInt("id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            Double averageMark = resultSet.getDouble("average_mark");

            result.add(id + "\t" + firstName + "\t" + lastName + "\t" + averageMark);
        }

        statement.close();

        return result;
    }

    public void UpdateById(int id, String firstName, String lastName, double averageMark) throws SQLException {
        String query = "{CALL update_by_id(" + id + ",'" + firstName + "', '" + lastName + "', " + averageMark + ")}";
        CallableStatement statement = connection.prepareCall(query);
        statement.execute();
        statement.close();
    }

    public ArrayList<String> SelectAllFromTable() throws SQLException {
        ArrayList<String> result = new ArrayList<>();
        Statement statement = connection.createStatement();
        String query = "{CALL select_all_from_table()}";
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next())
        {
            int id = resultSet.getInt("id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            Double averageMark = resultSet.getDouble("average_mark");

            result.add(id + "\t" + firstName + "\t" + lastName + "\t" + averageMark);
        }

        statement.close();

        return result;
    }
}
