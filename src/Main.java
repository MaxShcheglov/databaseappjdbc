import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.ArrayList;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

public class Main {
    private JPanel mainPanel;
    private JButton createDbButton;
    private JButton dropDbButton;
    private JTextArea textArea1;
    private JButton addButton;
    private JButton clearButton;
    private JButton searchButton;
    private JButton updateButton;
    private JButton removeButton;
    private JLabel statusBarLabel;

    private static DatabaseLayer databaseLayer;

    public Main() throws SQLException {

        initializeDb();
        updateTextArea(databaseLayer.SelectAllFromTable());

        createDbButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    databaseLayer.CreateDatabase();
                    updateStatusBar("database created successfully");
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                    if (throwables.getMessage().equals("Table 'students' already exists")) {
                        updateStatusBar("table already exists");
                    } else {
                        updateStatusBar("an error occurred while creating the database");
                    }

                }
            }
        });

        dropDbButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    databaseLayer.DropDatabase();
                    updateTextArea(new ArrayList<>());
                    updateStatusBar("database dropped successfully");
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                    updateStatusBar("an error occurred while dropping the database");
                }
            }
        });

        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    JFrame frame = new JFrame();
                    String input =JOptionPane.showInputDialog(frame,"Enter firstName,lastName,averageMark to insert");
                    String[] parameters = input.split(",");
                    databaseLayer.InsertIntoTable(parameters[0], parameters[1], parseDouble(parameters[2]));
                    updateTextArea(databaseLayer.SelectAllFromTable());
                    updateStatusBar("data added successfully");
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                    updateStatusBar("an error occurred while adding the data");
                }
            }
        });

        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    databaseLayer.TruncateTable();
                    updateTextArea(databaseLayer.SelectAllFromTable());
                    updateStatusBar("table truncated successfully");
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                    updateStatusBar("an error occurred while truncating the table");
                }
            }
        });

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame f=new JFrame();
                String input =JOptionPane.showInputDialog(f,"Enter lastName to search");
                try {
                    updateTextArea(databaseLayer.SelectByLastName(input));
                    updateStatusBar("search completed successfully");
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                    updateStatusBar("an error occurred while searching the record");
                }
            }
        });

        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame f=new JFrame();
                String input =JOptionPane.showInputDialog(f,"Enter id,firstName,lastName,averageMark to update");
                String[] parameters = input.split(",");
                try {
                    databaseLayer.UpdateById(parseInt(parameters[0]), parameters[1], parameters[2], parseDouble(parameters[3]));
                    updateTextArea(databaseLayer.SelectAllFromTable());
                    updateStatusBar("update completed successfully");
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                    updateStatusBar("an error occurred while updating the record");
                }
            }
        });

        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame f=new JFrame();
                String input =JOptionPane.showInputDialog(f,"Enter id to remove");
                try {
                    databaseLayer.DeleteById(parseInt(input));
                    updateTextArea(databaseLayer.SelectAllFromTable());
                    updateStatusBar("remove completed successfully");
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                    updateStatusBar("an error occurred while removing the record");
                }
            }
        });
    }

    private void updateStatusBar(String message) {
        statusBarLabel.setText(message);
    }

    private void updateTextArea(ArrayList<String> data) {
        String text = "";
        for (String record : data) {
            text = text += record + "\r\n";
        }
        textArea1.setText(text);
    }

    private static void initializeDb() {
        try {
            databaseLayer = new DatabaseLayer();
            databaseLayer.Initialize();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static void terminateDb() {
        try {
            databaseLayer.Terminate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException {
        JFrame frame = new JFrame("JDBC Sample app");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(new Main().mainPanel);
        frame.pack();
        frame.setLocationRelativeTo(null); // center screen
        frame.setVisible(true);

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                if (JOptionPane.showConfirmDialog(null,
                        "Are you sure you want to close this window?", "Close Window?",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
                    terminateDb();
                    System.exit(0);
                }
            }
        });
    }
}
